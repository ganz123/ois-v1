import java.util.*;
public class Gravitacija{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        double ndm = sc.nextDouble();
        double a = izracun(ndm);
        izpis(a,ndm);
    }

    public static double izracun(double ndm){
        double C = 6.674*Math.pow(10,-11);
        double M = 5.972*Math.pow(10,24);
        double R = 6.371*Math.pow(10,6);
        double a = C*M/(Math.pow((R+ndm),2));
        return a;
    }
        
    
    public static void izpis(double a,double ndm){
        System.out.println(a);
        System.out.println(ndm);

    }
}